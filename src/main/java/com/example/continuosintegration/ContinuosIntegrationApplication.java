package com.example.continuosintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContinuosIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContinuosIntegrationApplication.class, args);
	}

}
